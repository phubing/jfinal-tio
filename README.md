# jfinal-tio

#### 项目介绍
jfinal整合tio

#### 软件架构
JFinal3.5
Tio3.2.1.v20181024-RELEASE


#### 安装教程

1. git clone https://gitee.com/xiaoxustudent/jfinal-tio


#### 使用说明

1. 右键运行MainConifg
2. 发送消息给服务端：http://localhost/helloserver
3. 发送消息给客户端：http://localhost/helloclient
4. 绑定userId参考：com/seven/server/HelloServerAioHandler.java:107

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)